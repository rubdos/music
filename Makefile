LILY=$(wildcard **/*.ly)
PDFS=$(patsubst %.ly,%.pdf,$(LILY))

all: $(PDFS)

%.pdf: %.ly
	lilypond -o $* $<

%.midi: %.ly
	lilymidi $<
