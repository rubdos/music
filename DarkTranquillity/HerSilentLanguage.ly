\version "2.18.2"

\header {
  title = "Her Silent Language"
  composer = "Dark Traquillity"
  tagline = ##f
  subtitle = "Arr. Ruben De Smet"
}

intro = <<
  % Intro
  \context Voice = "vocal" \repeat volta 2 { r1 r1 r1 r1 }
  \context PianoStaff = "piano" \repeat volta 2 <<
    \context ChordNames = "chords" \chordmode {fis1:m cis:m b:m a}
    \context Staff = "upper" \relative c {
      fis'8[ cis]~cis cis4 cis8 cis fis |
      e8 cis~cis cis4 cis8 b cis |
      cis8 d~d d4 d8 cis b |
      b cis8~cis cis4 a'8 gis e
    }
    \context Staff = "lower" \relative c {
      fis1 cis b a
    }
  >>
>>

strofe_first = <<
  % First strofe
  \context Voice = "vocal" \relative c' {
    \set Staff.midiInstrument = "marimba"
    r1 r4 a' a8 a4 a8~ | a8 fis4. r4
    r8 fis8 | a a4 a4 a4 b8~ | b4
    cis cis8 cis~ cis cis8~ | cis8 b4 r8 r2 |
    r2 cis4 cis4 | e8 e4 r8 r4 e8 fis |
    fis8( cis8) ~ cis4 r2
  }

  \context PianoStaff = "piano" <<
    \context ChordNames = "chords"  \chordmode {
      fis1:m a fis:m a cis:m
      b:m cis:m e
      fis:m
    }
    \context Staff = "upper" \relative c' {
      r1 r4 a' a8 a4 a8~ | a8 fis4. r4
      r8 fis8 | a a4 a4 a4 b8~ | b4
      < cis gis e > < cis gis e >8 < cis gis e >~ < cis gis e > < cis gis e >~ | < cis gis e >8 < b d, fis >4 r8 r2 |
      r2 < cis gis e cis>4 < cis gis e cis>4 | < e b gis >8 < e b gis >4 r8 r4
      r8 fis | < fis cis a >4 cis4 r8 cis8 cis fis
    }
    \context Staff = "lower" \relative c, {
      \repeat unfold 3 {fis8 fis16 fis} fis16 fis cis' cis
      \repeat unfold 3 {a8 a16 a} a8 a16 e
      \repeat unfold 3 {fis8 fis16 fis} fis16 fis cis' cis
      \repeat unfold 4 {a8 a16 a}
      \repeat unfold 4 {cis8 cis16 cis}
      \repeat unfold 4 {b8 b16 b}
      \repeat unfold 4 {cis8 cis16 cis}
      \repeat unfold 4 {e8 e16 e}
      \repeat unfold 3 {fis8 fis16 fis} fis16 fis cis' cis
    }
  >>
>>

refrain = <<
  % Refrain
  \context Voice = "vocal" \relative c' {
    \xNotesOn \set Staff.midiInstrument = "synth drum"
    \repeat volta 2 {
      r4 e8 e e e e e~ | e8 e8 ~ e4 r4
      \parenthesize e8 e8 | e4 e4 e8 e e e~ | e r8 r4 r2
    }
    \xNotesOff \set Staff.midiInstrument = "marimba"
  }

  \context Lyrics \lyricsto "vocal" {
    Have you come here to warn me,
    _ of what I can -- not _ see?
  }
  \context Lyrics = "lyrics2" \lyricsto "vocal" {
    You want to tell me some -- thing,
    but you do not have _ the words.
  }
  \context PianoStaff = "piano" <<
    \context ChordNames = "chords"  \chordmode {
      cis1:m e
    }
    \context Staff = "upper" \relative c'' {
      \repeat volta 2 {
        e8 cis~cis cis4 cis8 b cis |
        cis8 d~d d4 d8 cis b |
        b cis8~cis cis4 a'8 gis e |
      }
      \alternative {
        {
          fis8[ cis]~cis cis4 cis8 cis fis
        }
        {
        }
      }
    }
    \context Staff = "lower" \relative c {
      \repeat volta 2 {
        \repeat unfold 4 {cis8 cis16 cis}
        \repeat unfold 4 {b8 b16 b}
        \repeat unfold 4 {a8 a16 a}
      }
      \alternative {
        {\repeat unfold 4 {fis'8 fis16 fis}}
        { \repeat unfold 3 {fis8 fis16 fis} fis16 fis cis' cis}
      }
    }
  >>
>>

\score {
  <<
    \new Voice = "vocal" <<
      \set Voice.instrumentName = #"Voice  "
      \clef treble
      \key a \major
    >>
    \new Lyrics \lyricsto "vocal" {a}
    \new Lyrics = "lyrics2" \lyricsto "vocal" {a}

    \new PianoStaff = "piano" <<
      \set PianoStaff.instrumentName = #"Piano  "

      \new ChordNames = "chords" <<
        \key a \major
      >>

      \new Staff = "upper" <<
        \clef treble
        \key a \major
      >>
      \new Staff = "lower" <<
        \clef bass
        \key a \major
      >>
    >>

    {
      \intro
      \strofe_first
      \refrain
    }
    \context Lyrics \lyricsto "vocal" {
      Why do I see her, the ne -- ver end -- ing nights?
      Why do I see her, wear -- ing no -- thing but the dark_?
    }
  >>

  \layout {
    \context {
      \Staff
      \RemoveEmptyStaves
      \accepts "Lyrics"
    }
    \context {
      \Lyrics
      \consists "Bar_engraver"
    }
    \context {
      \PianoStaff
    }
  }
}

\score {
  \unfoldRepeats

  <<
    \new Voice = "vocal" {
      \set Voice.midiInstrument = #"marimba"
      \clef treble
      \key a \major
    }
    \new PianoStaff = "piano" <<
      \new Staff = "upper" {
        \set Staff.midiInstrument = #"acoustic grand"
        \clef treble
        \key a \major
      }

      \new Staff = "lower" {
        \set Staff.midiInstrument = #"acoustic grand"
        \clef bass
        \key a \major
      }

      \new ChordNames = "chords" <<
        \set ChordNames.midiInstrument = #"acoustic guitar (nylon)"
        \set ChordNames.midiMaximumVolume = #0.7
        \key a \major
      >>
    >>
    {
      \intro
      \strofe_first
      \refrain
    }
  >>

  \midi {
    \tempo 4 = 120
  }
}
